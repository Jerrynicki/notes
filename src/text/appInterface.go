package text

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/types"
)

var (
	app AppInterface
)

type AppInterface interface {
	GetTextView() *gtk.TextView
	UpdateViewLabel(*types.View)
}

func LinkApp(app_ AppInterface) {
	app = app_
}
