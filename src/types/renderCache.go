package types

type RenderCache struct {
	LineLines []LineLine
	Shapes    []TwoPointObject
	Texts     []Text
	View      View

	LineLinesPage int
	ShapesPage    int
	TextsPage     int
}
