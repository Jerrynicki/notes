package drawing

import (
	"math"

	"github.com/gotk3/gotk3/cairo"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/page"
	"gitlab.com/Jerrynicki/notes/src/types"
)

var (
	renderCache *types.RenderCache
)

func OnDraw(darea *gtk.DrawingArea, cr *cairo.Context) {
	width := float64(darea.GetAllocatedWidth())
	height := float64(darea.GetAllocatedHeight())

	currentPage := page.GetCurrentPage()

	app.UpdateViewLabel(&currentPage.View)
	currentPage.BackgroundColor = app.GetSelectedBgColor()

	Render(cr, currentPage, page.GetCurrentSettings(), page.GetCurrentSettings().Tool == types.ToolEraser, &currentPage.View, width, height)
}

func rebuildRenderCache(page *types.Page, view *types.View, width float64, height float64) {
	if renderCache == nil {
		renderCache = new(types.RenderCache)
	}

	// TODO: Check if inbounds

	renderCache.LineLines = make([]types.LineLine, 0, len(page.LineLines))
	renderCache.Shapes = make([]types.TwoPointObject, 0, len(page.Shapes))
	renderCache.Texts = make([]types.Text, 0, len(page.Texts))

	hScreen := types.Hitbox{}
	hScreen.From2Coords(types.Coords2D{X: 0, Y: 0}, types.Coords2D{X: width, Y: height})

	h := types.Hitbox{}

	// Append all objects that are inbounds
	for _, x := range page.LineLines {
		h.FromCoordsAndSize(view.TransformCoords(x.Position), x.Size.Product(&types.Coords2D{X: view.Scale, Y: view.Scale}))
		if h.Intersects(&hScreen) {
			renderCache.LineLines = append(renderCache.LineLines, x)
		}
	}

	for _, x := range page.Shapes {
		h.From2Coords(view.TransformCoords(x.Start), view.TransformCoords(x.End))
		if h.Intersects(&hScreen) {
			renderCache.Shapes = append(renderCache.Shapes, x)
		}
	}

	for _, x := range page.Texts {
		h.FromCoordsAndSize(view.TransformCoords(x.Position), x.Size.Product(&types.Coords2D{X: view.Scale, Y: view.Scale}))
		if h.Intersects(&hScreen) {
			renderCache.Texts = append(renderCache.Texts, x)
		}
	}

	// Save length of vars at which they were synchronised
	renderCache.LineLinesPage = len(page.LineLines)
	renderCache.ShapesPage = len(page.Shapes)
	renderCache.TextsPage = len(page.Texts)

	renderCache.View = *view
}

func Render(cr *cairo.Context, page *types.Page, settings *types.Settings, renderHitboxes bool, view *types.View, width float64, height float64) {
	// Render cache
	if renderCache == nil || renderCache.View.Position != view.Position || renderCache.View.Scale != view.Scale {
		// If the render cache is not initialized or the view has been changed, it needs to be rebuilt
		rebuildRenderCache(page, view, width, height)
	} else {
		if len(page.LineLines) < renderCache.LineLinesPage || len(page.Shapes) < renderCache.ShapesPage || len(page.Texts) < renderCache.TextsPage {
			// if anything was erased -> rebuild (for now)
			rebuildRenderCache(page, view, width, height)
		} else {
			// If anything has been added but view not changed - no need to rebuild, just add it to the cache
			// No need to determine if in-bounds, since user will 99% of the time draw in-bounds anyway
			if len(page.LineLines) == renderCache.LineLinesPage+1 {
				renderCache.LineLines = append(renderCache.LineLines, page.LineLines[len(page.LineLines)-1])
				renderCache.LineLinesPage++
			}
			if len(page.Shapes) == renderCache.ShapesPage+1 {
				renderCache.Shapes = append(renderCache.Shapes, page.Shapes[len(page.Shapes)-1])
				renderCache.ShapesPage++
			}
			if len(page.Texts) == renderCache.TextsPage+1 {
				renderCache.Texts = append(renderCache.Texts, page.Texts[len(page.Texts)-1])
				renderCache.TextsPage++
			}
		}
	}

	// Background
	cr.SetSourceRGBA(page.BackgroundColor.ToFloat64())
	cr.Rectangle(0, 0, width, height)
	cr.Fill()

	// Grid
	// FIXME
	if settings.GridActive {
		zero := view.TransformCoordsBack(types.Coords2D{X: -settings.GridSize, Y: -settings.GridSize}) // make sure we draw points before 0 0 so they don't just phase into existence
		start := GridGetNearest(&zero, settings.GridSize)

		sizeT := view.TransformCoordsBack(types.Coords2D{X: width + settings.GridSize, Y: height + settings.GridSize})

		// Numbers of points per axis
		pointsX := int(math.Ceil(math.Abs(sizeT.X / settings.GridSize)))
		pointsY := int(math.Ceil(math.Abs(sizeT.Y / settings.GridSize)))

		cr.SetLineWidth(1)
		col := page.BackgroundColor.Inverted()
		r, g, b, _ := col.ToFloat64()
		cr.SetSourceRGBA(r, g, b, 0.25)

		for pX := 0; pX < pointsX; pX++ {
			c := view.TransformCoords(types.Coords2D{X: start.X + view.Scale*settings.GridSize*float64(pX), Y: start.Y})

			cr.MoveTo(c.X, c.Y)
			cr.LineTo(c.X, c.Y+height+(2*settings.GridSize/view.Scale))
			cr.Stroke()
		}

		for pY := 0; pY < pointsY; pY++ {
			c := view.TransformCoords(types.Coords2D{X: start.X, Y: start.Y + view.Scale*settings.GridSize*float64(pY)})

			cr.MoveTo(c.X, c.Y)
			cr.LineTo(c.X+width+(2*settings.GridSize/view.Scale), c.Y)
			cr.Stroke()
		}

		// cr.SetSourceRGBA(settings.GridColor.ToFloat64())

	}

	// Merge current in-progress-line
	if currentLineLine != nil {
		renderCache.LineLines = append(renderCache.LineLines, *currentLineLine)
	}

	// Lines
	for _, lineLine := range renderCache.LineLines {

		// If eraser is active, render hitboxes
		if renderHitboxes {
			cr.SetSourceRGBA(1, 0, 0, 0.5)
			t := view.TransformCoords(lineLine.Position)
			s := lineLine.Size.Product(&types.Coords2D{X: view.Scale, Y: view.Scale})
			cr.Rectangle(t.X, t.Y, s.X, s.Y)
			cr.Fill()
		}

		for i, point := range lineLine.Points {
			positionTransformed := view.TransformCoords(point.Position)

			cr.SetSourceRGBA(lineLine.Color.ToFloat64())
			cr.SetLineWidth(lineLine.Thickness * view.Scale)

			if i == 0 {
				cr.MoveTo(positionTransformed.X, positionTransformed.Y)
			} else {
				cr.LineTo(positionTransformed.X, positionTransformed.Y)
			}
		}
		cr.Stroke()
	}

	// Remove merged in-progress line
	if currentLineLine != nil {
		renderCache.LineLines = renderCache.LineLines[:len(renderCache.LineLines)-1]
	}

	// Merge current in-progress shape
	if currentShape != nil {
		renderCache.Shapes = append(renderCache.Shapes, *currentShape)
	}

	// Shapes
	for _, shape := range renderCache.Shapes {
		// if eraser is active, render hitboxes
		if renderHitboxes {
			cr.SetSourceRGBA(1, 0, 0, 0.5)
			t := view.TransformCoords(shape.Start)
			s := view.TransformCoords(shape.End)
			s = s.Subtract(&t)
			cr.Rectangle(t.X, t.Y, s.X, s.Y)
			cr.Fill()
		}

		positionStartTransformed := view.TransformCoords(shape.Start)
		positionEndTransformed := view.TransformCoords(shape.End)

		cr.SetSourceRGBA(shape.Color.ToFloat64())
		cr.SetLineWidth(shape.Thickness * view.Scale)

		switch shape.Type {
		case types.ToolLine:
			cr.MoveTo(positionStartTransformed.X, positionStartTransformed.Y)
			cr.LineTo(positionEndTransformed.X, positionEndTransformed.Y)
		case types.ToolArrow:
			const tipAngle = math.Pi / 4
			const rawTipDist = 7.5

			d := positionEndTransformed.Subtract(&positionStartTransformed)
			angle := math.Atan2(d.X, d.Y)

			tipDist := rawTipDist * shape.Thickness

			cr.MoveTo(positionStartTransformed.X, positionStartTransformed.Y)
			cr.LineTo(positionEndTransformed.X, positionEndTransformed.Y)

			cr.LineTo(positionEndTransformed.X+tipDist*view.Scale*math.Cos(angle+tipAngle), positionEndTransformed.Y-tipDist*view.Scale*math.Sin(angle+tipAngle))
			cr.MoveTo(positionEndTransformed.X, positionEndTransformed.Y)
			cr.LineTo(positionEndTransformed.X-tipDist*view.Scale*math.Cos(angle-tipAngle), positionEndTransformed.Y+tipDist*view.Scale*math.Sin(angle-tipAngle))

		case types.ToolCircleEnds:
			dist := positionStartTransformed.DistanceFrom(&positionEndTransformed)
			distVec := positionEndTransformed.Subtract(&positionStartTransformed)
			middle := positionStartTransformed.Add(&types.Coords2D{X: distVec.X / 2, Y: distVec.Y / 2})

			wScale := math.Abs(distVec.X / distVec.Y)
			hScale := 1 / wScale

			cr.Save()

			cr.Translate(middle.X, middle.Y)
			cr.Scale(wScale, hScale)

			cr.Arc(
				0,
				0,
				dist/2/math.Max(wScale, hScale),
				0,
				2*math.Pi,
			)

			cr.Restore()
		case types.ToolCircleCenter:
			radius := positionStartTransformed.DistanceFrom(&positionEndTransformed)

			cr.Arc(
				positionStartTransformed.X,
				positionStartTransformed.Y,
				radius,
				0,
				2*math.Pi,
			)

		case types.ToolRectangle:
			dist := positionEndTransformed.Subtract(&positionStartTransformed)
			cr.Rectangle(positionStartTransformed.X, positionStartTransformed.Y, dist.X, dist.Y)
		}

		cr.Stroke()
	}

	// Remove merged in-progress shape
	if currentShape != nil {
		renderCache.Shapes = renderCache.Shapes[:len(renderCache.Shapes)-1]
	}

	// Texts

	allTexts := renderCache.Texts

	// Line for current text, add to texts
	if currentText != nil {
		positionTransformed := view.TransformCoords(currentText.Position)
		cr.MoveTo(positionTransformed.X, positionTransformed.Y)
		cr.SetSourceRGBA(currentText.Color.ToFloat64())
		cr.SetLineWidth(2)
		cr.LineTo(positionTransformed.X, positionTransformed.Y-float64(currentText.Font.Height))
		cr.Stroke()

		allTexts = append(allTexts, *currentText)
	}

	// Text rendering
	// TODO: Switch to the more epic cairo text API which might even support uppercase characters and newlines (pog!)
	for _, text := range allTexts {
		positionTransformed := view.TransformCoords(text.Position)
		cr.SetSourceRGBA(text.Color.ToFloat64())
		cr.MoveTo(positionTransformed.X, positionTransformed.Y)
		cr.SelectFontFace(text.Font.Name, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
		cr.SetFontSize(float64(text.Font.Height) * view.Scale)
		cr.ShowText(text.Label)
	}
}
