package types

type Page struct {
    // Contains all the information about the page in one struct
    LineLines       []LineLine
    Shapes          []TwoPointObject
    Texts           []Text
    Smallest        Coords2D
    Size            Coords2D
    BackgroundColor Color

    Writing string

    View View
}

func (page *Page) FindSize() {
    // Fill the Smallest and Size attributes

    var tl Coords2D
    var br Coords2D
    if len(page.LineLines) > 0 {
        tl = page.LineLines[0].Position
        br = page.LineLines[0].Position
    } else if len(page.Shapes) > 0 {
        tl = page.Shapes[0].Start
        br = page.Shapes[0].End
    } else if len(page.Texts) > 0 {
        tl = page.Texts[0].Position
        br = page.Texts[0].Position
    }

    for _, x := range page.LineLines {
        if x.Position.X < tl.X {
            tl.X = x.Position.X
        }
        if x.Position.Y < tl.Y {
            tl.Y = x.Position.Y
        }

        if x.Position.X+x.Size.X > br.X {
            br.X = x.Position.X + x.Size.X
        }
        if x.Position.Y+x.Size.Y > br.Y {
            br.Y = x.Position.Y + x.Size.Y
        }
    }
    for _, x := range page.Shapes {
        if x.Start.X < tl.X {
            tl.X = x.Start.X
        }
        if x.Start.Y < tl.Y {
            tl.Y = x.Start.Y
        }

        if x.End.X > br.X {
            br.X = x.End.X
        }
        if x.End.Y > br.Y {
            br.Y = x.End.Y
        }
    }
    for _, x := range page.Texts {
        if x.Position.X < tl.X {
            tl.X = x.Position.X
        }
        if x.Position.Y < tl.Y {
            tl.Y = x.Position.Y
        }

        if x.Position.X+x.Size.X > br.X {
            br.X = x.Position.X + x.Size.X
        }
        if x.Position.Y+x.Size.Y > br.Y {
            br.Y = x.Position.Y + x.Size.Y
        }
    }

    page.Smallest = tl
    page.Size = br.Subtract(&tl)
}
