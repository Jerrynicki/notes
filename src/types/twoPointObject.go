package types

type TwoPointObject struct {
	Start     Coords2D
	End       Coords2D
	Color     Color
	Type      int // corresponds to the types.Tools enum
	Thickness float64
}
