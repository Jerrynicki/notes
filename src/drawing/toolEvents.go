package drawing

import (
	"fmt"

	"github.com/gotk3/gotk3/gdk"
	"gitlab.com/Jerrynicki/notes/src/page"
	"gitlab.com/Jerrynicki/notes/src/types"
)

const (
	KEY_ENTER = 65293
	KEY_ESC   = 65307
)

func ToolButtonEvent(t int) {
	page.GetCurrentSettings().Tool = t
	app.QueueDrawAreaDraw()
}

func MotionPen(x float64, y float64) {
	currentPage := page.GetCurrentPage()
	currentSettings := page.GetCurrentSettings()

	var transformed types.Coords2D
	if page.GetCurrentSettings().GridSnap {
		transformed = currentPage.View.TransformCoordsBack(types.Coords2D{X: x, Y: y})
		transformed = GridGetNearest(&transformed, page.GetCurrentSettings().GridSize)
	} else {
		transformed = currentPage.View.TransformCoordsBack(types.Coords2D{X: x, Y: y})
	}

	if currentLineLine == nil { // begin new lineline if none is currently being worked on
		currentLineLine = new(types.LineLine)
		currentLineLine.Thickness = app.GetSelectedThickness()
		currentLineLine.Color = app.GetSelectedColor()
	} else {
		if currentLineLine.Points[len(currentLineLine.Points)-1].Position.DistanceFrom(&transformed) < float64(currentSettings.MinimumPointDistance) {
			// if current point and last point are not further apart than the minimum point distance, don't save this point
			return
		}
	}

	lp := new(types.LinePoint)
	lp.Position = transformed

	currentLineLine.Points = append(currentLineLine.Points, *lp)

	app.UpdateBottomLabel2(fmt.Sprintf("Distance: %.1f", currentLineLine.Points[0].Position.DistanceFrom(&transformed)))
}

func MotionOther(x float64, y float64, t int, doneDrawing bool) {
	currentPage := page.GetCurrentPage()

	var transformed types.Coords2D
	var start types.Coords2D
	if page.GetCurrentSettings().GridSnap {
		t := currentPage.View.TransformCoordsBack(types.Coords2D{X: x, Y: y})
		transformed = GridGetNearest(&t, page.GetCurrentSettings().GridSize)

		s := currentPage.View.TransformCoordsBack(*lastPrimaryButtonPosition)
		start = GridGetNearest(&s, page.GetCurrentSettings().GridSize)
	} else {
		transformed = currentPage.View.TransformCoordsBack(types.Coords2D{X: x, Y: y})
		start = currentPage.View.TransformCoordsBack(*lastPrimaryButtonPosition)
	}

	if lastPrimaryButtonPosition != nil {
		if currentShape == nil {
			currentShape = &types.TwoPointObject{
				Start:     start,
				End:       transformed,
				Color:     app.GetSelectedColor(),
				Thickness: app.GetSelectedThickness(),
				Type:      t,
			}
		} else {
			currentShape.End = transformed
		}
	}

	app.UpdateBottomLabel2(fmt.Sprintf("Distance: %.1f", currentShape.Start.DistanceFrom(&transformed)))
}

func ClickText(x float64, y float64) {
	currentPage := page.GetCurrentPage()

	if currentText == nil {
		currentText = new(types.Text)
		currentText.Color = app.GetSelectedColor()
		currentText.Font.FromString(app.GetSelectedFont())
		currentText.Position = currentPage.View.TransformCoordsBack(types.Coords2D{X: x, Y: y})
	}
}

func KeyText(key *gdk.EventKey) {
	currentPage := page.GetCurrentPage()

	if currentText != nil {
		switch key.KeyVal() {
		case KEY_ENTER:
			currentText.Label += "\n"
		case KEY_ESC:
			currentPage.Texts = append(currentPage.Texts, *currentText)
			currentText = nil
		default:
			c := gdk.KeyvalToUnicode(key.KeyVal())
			currentText.Label += string(c)
		}
	}
}

func MotionEraser(x float64, y float64) {
	currentPage := page.GetCurrentPage()

	c := currentPage.View.TransformCoordsBack(types.Coords2D{X: x, Y: y})

	for li, lineLine := range currentPage.LineLines {
		hitbox := new(types.Hitbox)
		hitbox.FromCoordsAndSize(lineLine.Position, lineLine.Size)
		if hitbox.Contains(&c) {
			currentPage.LineLines[li] = currentPage.LineLines[len(currentPage.LineLines)-1]
			currentPage.LineLines = currentPage.LineLines[:len(currentPage.LineLines)-1]
			break
		}
	}

	for si, shape := range currentPage.Shapes {
		hitbox := new(types.Hitbox)
		hitbox.From2Coords(shape.Start, shape.End)
		if hitbox.Contains(&c) {
			currentPage.Shapes[si] = currentPage.Shapes[len(currentPage.Shapes)-1]
			currentPage.Shapes = currentPage.Shapes[:len(currentPage.Shapes)-1]
			return
		}
	}
}
