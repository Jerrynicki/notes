package drawing

import (
	"math"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/page"
	"gitlab.com/Jerrynicki/notes/src/types"
)

const (
	viewScaleScrollSpeed = 0.10
)

var (
	currentShape    *types.TwoPointObject
	currentLineLine *types.LineLine
	currentText     *types.Text

	IsPrimaryButtonHeld   bool
	IsSecondaryButtonHeld bool

	lastPrimaryButtonPosition   *types.Coords2D
	lastSecondaryButtonPosition *types.Coords2D
)

func MotionNotifyEvent(widget *gtk.DrawingArea, rawEvent *gdk.Event) {
	event := &gdk.EventMotion{Event: rawEvent}
	x, y := event.MotionVal()

	eventCoords := types.Coords2D{X: x, Y: y}

	currentPage := page.GetCurrentPage()
	currentSettings := page.GetCurrentSettings()

	if IsPrimaryButtonHeld {
		switch currentSettings.Tool {
		case types.ToolPen:
			MotionPen(x, y)
		case types.ToolEraser:
			MotionEraser(x, y)
		default:
			MotionOther(x, y, currentSettings.Tool, false)
		}

	}
	if IsSecondaryButtonHeld {
		if lastSecondaryButtonPosition != nil {
			moveViewBy := eventCoords.Subtract(lastSecondaryButtonPosition)                                                // delta from last position
			moveViewBy = moveViewBy.Product(&types.Coords2D{X: 1 / currentPage.View.Scale, Y: 1 / currentPage.View.Scale}) // prevent faster movement when more zoomed in
			currentPage.View.Position = currentPage.View.Position.Subtract(&moveViewBy)                                    // move the view
		}

		lastSecondaryButtonPosition = &eventCoords
	}

	if IsPrimaryButtonHeld || IsSecondaryButtonHeld {
		app.QueueDrawAreaDrawUnimportant()
	}
}

func ButtonPressEvent(widget *gtk.DrawingArea, rawEvent *gdk.Event) {
	event := &gdk.EventButton{Event: rawEvent} // workaround because gotk is weird

	if event.Button() == gdk.BUTTON_PRIMARY { // Drawing
		IsPrimaryButtonHeld = true
		lastPrimaryButtonPosition = &types.Coords2D{X: event.X(), Y: event.Y()}
	}
	if event.Button() == gdk.BUTTON_SECONDARY { // Moving view
		IsSecondaryButtonHeld = true
	}

	app.DAreaUpdateCursor(widget)
}

func ButtonReleaseEvent(widget *gtk.DrawingArea, rawEvent *gdk.Event) {
	event := &gdk.EventButton{Event: rawEvent} // workaround because gotk is weird

	if event.Button() == gdk.BUTTON_PRIMARY { // Drawing
		IsPrimaryButtonHeld = false

		currentPage := page.GetCurrentPage()
		currentSettings := page.GetCurrentSettings()

		switch currentSettings.Tool {
		case types.ToolText:
			ClickText(event.X(), event.Y())
		case types.ToolEraser:
			MotionEraser(event.X(), event.Y())
		case types.ToolPen:

		default:
			MotionOther(event.X(), event.Y(), currentSettings.Tool, true)
		}

		if currentLineLine != nil && len(currentLineLine.Points) != 0 {
			// mouse button is released -> Finish current LineLine and add it to the page
			currentLineLine.FindPosition()
			currentLineLine.FindSize()
			currentPage.LineLines = append(currentPage.LineLines, *currentLineLine)
			currentLineLine = nil
		}
		if currentShape != nil {
			currentPage.Shapes = append(currentPage.Shapes, *currentShape)
			currentShape = nil
		}

	}
	if event.Button() == gdk.BUTTON_SECONDARY { // Moving view
		IsSecondaryButtonHeld = false
		lastSecondaryButtonPosition = nil
	}

	if event.Button() == gdk.BUTTON_SECONDARY || event.Button() == gdk.BUTTON_PRIMARY {
		app.QueueDrawAreaDraw()
		app.DAreaUpdateCursor(widget)
	}
}

func ScrollEvent(widget *gtk.DrawingArea, rawEvent *gdk.Event) {
	event := &gdk.EventScroll{Event: rawEvent} // workaround because gotk is weird

	currentPage := page.GetCurrentPage()

	if event.Direction() == gdk.SCROLL_UP {
		currentPage.View.Scale += viewScaleScrollSpeed

	} else if event.Direction() == gdk.SCROLL_DOWN {
		currentPage.View.Scale = math.Max(0.1, currentPage.View.Scale-viewScaleScrollSpeed)
	}

	app.QueueDrawAreaDraw()
}

func KeyEvent(widget *gtk.DrawingArea, rawEvent *gdk.Event) {
	event := &gdk.EventKey{Event: rawEvent}

	currentSettings := page.GetCurrentSettings()

	if currentSettings.Tool == types.ToolText {
		KeyText(event)
	}

	app.QueueDrawAreaDraw()
}
