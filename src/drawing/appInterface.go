package drawing

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/types"
)

var (
	app AppInterface
)

type AppInterface interface {
	UpdateViewLabel(view *types.View)
	GetSelectedColor() types.Color
	GetSelectedBgColor() types.Color
	GetSelectedThickness() float64
	GetSelectedFont() string
	QueueDrawAreaDraw()
	QueueDrawAreaDrawUnimportant()
	DAreaUpdateCursor(widget *gtk.DrawingArea)
	UpdateBottomLabel2(s string)
}

func LinkApp(a AppInterface) {
	app = a
}
