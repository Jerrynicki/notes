package filehandling

import (
	"encoding/gob"
	"os"

	"gitlab.com/Jerrynicki/notes/src/types"
)

func LoadPageFromFilename(filename string) (*types.Page, error) {
	var page types.Page

	file, err := os.Open(filename)

	if err != nil {
		return nil, err
	}

	decoder := gob.NewDecoder(file)
	decoder.Decode(&page)

	err = file.Close()
	if err != nil {
		return nil, err
	}

	return &page, nil
}
