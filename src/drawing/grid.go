package drawing

import (
	"math"

	"gitlab.com/Jerrynicki/notes/src/types"
)

func GridGetNearest(c *types.Coords2D, gridSize float64) types.Coords2D {
	// Returns the nearest point on the grid
	xMod := math.Mod(c.X, gridSize)
	yMod := math.Mod(c.Y, gridSize)

	var xNext float64
	var yNext float64

	if xMod > gridSize/2 {
		xNext = c.X + (gridSize - xMod)
	} else {
		xNext = c.X - xMod
	}

	if yMod > gridSize/2 {
		yNext = c.Y + (gridSize - yMod)
	} else {
		yNext = c.Y - yMod
	}

	return types.Coords2D{X: xNext, Y: yNext}
}
