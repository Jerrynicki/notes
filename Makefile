default:
	mkdir -p build
	go build -o build/notes src/*.go
	chmod +x build/notes
	cp -r src/glade build/
	zip -r "build.zip" build
	rm -r build

start:
	cd src
	go build -o notes main.go
	./notes