package types

import (
	"strconv"
	"strings"
)

type Font struct {
	Name   string
	Height int
}

func (font *Font) ToString() string {
	return font.Name + " " + strconv.Itoa(font.Height)
}

func (font *Font) FromString(s string) {
	sSplit := strings.Split(s, " ")
	font.Name = sSplit[0]
	font.Height, _ = strconv.Atoi(sSplit[1])
}
