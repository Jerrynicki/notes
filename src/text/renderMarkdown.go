package text

import (
    "fmt"
    "math"
    "strings"

    "github.com/gotk3/gotk3/gtk"
    "github.com/gotk3/gotk3/pango"
)

func runeTimes(r rune, n int) string { // Repeats a rune n times and returns the resulting string
    var result strings.Builder

    for i := 0; i < n; i++ {
        result.WriteRune(r)
    }

    return result.String()
}

func InitTags(buf *gtk.TextBuffer) {
    boldMap := make(map[string]interface{})
    boldMap["weight"] = pango.WEIGHT_BOLD
    buf.CreateTag("bold", boldMap)

    italicMap := make(map[string]interface{})
    italicMap["style"] = pango.STYLE_ITALIC
    buf.CreateTag("italic", italicMap)

    monospaceMap := make(map[string]interface{})
    monospaceMap["family"] = "monospace"
    buf.CreateTag("monospace", monospaceMap)

    underlineMap := make(map[string]interface{})
    underlineMap["underline"] = pango.UNDERLINE_SINGLE
    buf.CreateTag("underline", underlineMap)

    strikethroughMap := make(map[string]interface{})
    strikethroughMap["strikethrough"] = pango.ATTR_STRIKETHROUGH
    buf.CreateTag("strikethrough", strikethroughMap)

    h1Map := make(map[string]interface{})
    h1Map["scale"] = 2.0
    h1Map["weight"] = pango.WEIGHT_BOLD
    buf.CreateTag("h1", h1Map)

    h2Map := make(map[string]interface{})
    h2Map["scale"] = 1.5
    h2Map["weight"] = pango.WEIGHT_BOLD
    buf.CreateTag("h2", h2Map)

    h3Map := make(map[string]interface{})
    h3Map["scale"] = 1.25
    h3Map["weight"] = pango.WEIGHT_BOLD
    buf.CreateTag("h3", h3Map)

    invisibleMap := make(map[string]interface{})
    invisibleMap["scale"] = 0
    buf.CreateTag("invisible", invisibleMap)

    SetGlobalScale(1.0, buf)
}

func SetGlobalScale(scale float64, buf *gtk.TextBuffer) {
    tt, _ := buf.GetTagTable()
    tag, err := tt.Lookup("global_scale")
    if err == nil { // re-init tag by deleting it beforehand
        tt.Remove(tag)
    }

    m := make(map[string]interface{})
    m["scale"] = scale
    buf.CreateTag("global_scale", m)
}

func makeInvisible(buf *gtk.TextBuffer, start [2]int, end [2]int, l int) {
    buf.ApplyTagByName("invisible", buf.GetIterAtLineIndex(start[0], start[1]), buf.GetIterAtLineIndex(start[0], start[1]+l))
    buf.ApplyTagByName("invisible", buf.GetIterAtLineIndex(end[0], end[1]-l), buf.GetIterAtLineIndex(end[0], end[1]))
}

func RenderMarkdown(widget *gtk.TextView, buf *gtk.TextBuffer) {
    var eol int
    var last rune
    var sameCounter int
    lastOccurence := make(map[string][2]int) // characters->[2]int{line num, offset in line}

    text, _ := buf.GetText(buf.GetStartIter(), buf.GetEndIter(), true)
    textSplit := strings.Split(text, "\n")
    buf.RemoveAllTags(buf.GetStartIter(), buf.GetEndIter())

    buf.ApplyTagByName("global_scale", buf.GetStartIter(), buf.GetEndIter())

    /*
       **text** = bold
       *text* = italic
       __text__ = underline
       ~~text~~ = strikethrough
       `text` = monospace
       #text = heading 1 (entire line)
       ##text = heading 2 (entire line)
       ###text = heading 3 (entire line)
    */

    for il, xl := range textSplit {
        sameCounter = 0

        xl = fmt.Sprintf("%s\n", xl) // workaround to prevent issue of markdown at the end of the line not being rendered
        eol = len(xl)

        markersInvisible := true
        start, end, _ := buf.GetSelectionBounds()
        if start != nil && end != nil {
            if start.GetLine() == il || end.GetLine() == il {
                markersInvisible = false
            }
        }

        for i, x := range xl {
            if last == '\\' {
                sameCounter = 0
                last = '\n'
                continue
            }

            if x == last { // if current rune is the same as the last one, keep count of how many there are
                sameCounter++
            } else {
                repeated := runeTimes(last, sameCounter+1)

                if start, ok := lastOccurence[repeated]; ok { // if this is true, this is the closing chars

                    // apply tags based on runes
                    switch repeated {
                    case "**":
                        buf.ApplyTagByName("bold", buf.GetIterAtLineIndex(start[0], start[1]), buf.GetIterAtLineIndex(il, i))
                        if markersInvisible {
                            makeInvisible(buf, start, [2]int{il, i}, len(repeated))
                        }
                    case "*":
                        buf.ApplyTagByName("italic", buf.GetIterAtLineIndex(start[0], start[1]), buf.GetIterAtLineIndex(il, i))
                        if markersInvisible {
                            makeInvisible(buf, start, [2]int{il, i}, len(repeated))
                        }
                    case "__":
                        buf.ApplyTagByName("underline", buf.GetIterAtLineIndex(start[0], start[1]), buf.GetIterAtLineIndex(il, i))
                        if markersInvisible {
                            makeInvisible(buf, start, [2]int{il, i}, len(repeated))
                        }
                    case "~~":
                        buf.ApplyTagByName("strikethrough", buf.GetIterAtLineIndex(start[0], start[1]), buf.GetIterAtLineIndex(il, i))
                        if markersInvisible {
                            makeInvisible(buf, start, [2]int{il, i}, len(repeated))
                        }
                    case "`":
                        buf.ApplyTagByName("monospace", buf.GetIterAtLineIndex(start[0], start[1]), buf.GetIterAtLineIndex(il, i))
                        if markersInvisible {
                            makeInvisible(buf, start, [2]int{il, i}, len(repeated))
                        }
                    }

                    delete(lastOccurence, repeated)
                } else { // opening chars
                    if last == '#' { // # headings
                        if i-sameCounter == 1 { // only if the # started at the beginning of the line
                            buf.ApplyTagByName(fmt.Sprintf("h%.0f", math.Min(3, float64(sameCounter+1))), buf.GetIterAtLineIndex(il, 0), buf.GetIterAtLineIndex(il, eol))

                            if markersInvisible {
                                add := 1

                                if buf.GetIterAtLineIndex(il, sameCounter+1).GetChar() == ' ' { // also hide space after #
                                    add = 2
                                }

                                buf.ApplyTagByName("invisible", buf.GetIterAtLineIndex(il, 0), buf.GetIterAtLineIndex(il, sameCounter+add))
                            }
                        }
                    } else { // if the runes are not #, which can only be used once anyway, save their starting offset
                        lastOccurence[repeated] = [2]int{il, int(math.Max(float64(i-len(repeated)), 0))}
                    }
                }

                sameCounter = 0
                last = x
            }
        }
        // reset on linebreak
        sameCounter = 0
        last = '\n'
        for k := range lastOccurence {
            delete(lastOccurence, k)
        }
    }
}
