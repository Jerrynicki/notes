package filehandling

import (
	"encoding/gob"
	"fmt"
	"os"

	"gitlab.com/Jerrynicki/notes/src/types"
)

func SavePageToFilename(page *types.Page, filename string) error {
	fmt.Println("savepagetofilename")
	file, err := os.Create(filename)
	if err != nil {
		return err
	}

	defer file.Close()

	encoder := gob.NewEncoder(file)
	err = encoder.Encode(*page)
	if err != nil {
		return err
	}

	return nil
}

func SavePageToCurrentFile(page *types.Page) error {
	_, err := os.Stat(currentFile)
	exists := err == nil

	if currentFile == "" || !exists {
		return err
	}

	return SavePageToFilename(page, currentFile)
}
