package gui

import (
	"fmt"
	"io"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/gotk3/gotk3/cairo"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"

	"gitlab.com/Jerrynicki/notes/src/drawing"
	"gitlab.com/Jerrynicki/notes/src/filehandling"
	"gitlab.com/Jerrynicki/notes/src/page"
	"gitlab.com/Jerrynicki/notes/src/text"
	"gitlab.com/Jerrynicki/notes/src/types"
)

func (app *App) SavePage(window *gtk.ApplicationWindow) {
	err := filehandling.SavePageToCurrentFile(page.GetCurrentPage())
	if err != nil {
		app.SavePageAs(window)
	}

	obj, _ := app.builder.GetObject("bottomLabel")
	obj.(*gtk.Label).SetLabel("Saved.")
	glib.TimeoutAdd(1000, func() { app.UpdateViewLabel(&page.GetCurrentPage().View) })
}

func (app *App) SavePageAs(window *gtk.ApplicationWindow) {
	dialog, _ := gtk.FileChooserDialogNewWith2Buttons(
		"Choose file",
		app.window,
		gtk.FILE_CHOOSER_ACTION_SAVE,
		"Cancel",
		gtk.RESPONSE_CANCEL,
		"Save",
		gtk.RESPONSE_OK,
	)

	wd, err := os.Getwd()
	if err != nil {
		fmt.Println("couldnt get wd (how???)")
		return
	}
	dialog.FileChooser.SetCurrentFolder(wd)

	filter1, _ := gtk.FileFilterNew()
	filter1.SetName("Notes files (*.note)")
	filter1.AddPattern("*.note")
	dialog.AddFilter(filter1)

	filter2, _ := gtk.FileFilterNew()
	filter2.SetName("All files")
	filter2.AddPattern("*")
	dialog.AddFilter(filter2)

	defer dialog.Destroy()

	if dialog.Run() == gtk.RESPONSE_OK {
		filename := dialog.GetFilename()
		if !strings.HasSuffix(filename, ".note") {
			filename += ".note"
		}

		filehandling.SavePageToFilename(page.GetCurrentPage(), filename) // TODO: error handling
		filehandling.SetCurrentFile(filename)
		filehandling.SetCurrentDir(path.Dir(filename))
		app.UpdateFileList()
	}
}

func (app *App) LoadPage(window *gtk.ApplicationWindow) {
	dialog, _ := gtk.FileChooserDialogNewWith2Buttons(
		"Choose file",
		app.window,
		gtk.FILE_CHOOSER_ACTION_OPEN,
		"Cancel",
		gtk.RESPONSE_CANCEL,
		"Open",
		gtk.RESPONSE_OK,
	)

	filter1, _ := gtk.FileFilterNew()
	filter1.SetName("Notes files (*.note)")
	filter1.AddPattern("*.note")
	dialog.AddFilter(filter1)

	filter2, _ := gtk.FileFilterNew()
	filter2.SetName("All files")
	filter2.AddPattern("*")
	dialog.AddFilter(filter2)

	wd, err := os.Getwd()
	if err != nil {
		fmt.Println("couldnt get wd (how???)")
		return
	}
	dialog.FileChooser.SetCurrentFolder(wd)

	defer dialog.Destroy()

	if dialog.Run() == gtk.RESPONSE_OK {
		filename := dialog.GetFilename()
		newPage, err := filehandling.LoadPageFromFilename(filename)
		if err != nil { // TODO: error handling
			return
		}

		page.SetCurrentPage(newPage)
		text.PageLoaded(newPage.Writing, newPage.View.Scale)
		filehandling.SetCurrentFile(filename)
		filehandling.SetCurrentDir(path.Dir(filename))
		app.UpdateFileList()
		app.QueueDrawAreaDraw()
	}
}

func (app *App) ImportMD() {
	dialog, _ := gtk.FileChooserDialogNewWith2Buttons(
		"Choose file",
		app.window,
		gtk.FILE_CHOOSER_ACTION_OPEN,
		"Cancel",
		gtk.RESPONSE_CANCEL,
		"Open",
		gtk.RESPONSE_OK,
	)

	filter1, _ := gtk.FileFilterNew()
	filter1.SetName("Markdown files (*.md)")
	filter1.AddPattern("*.md")
	dialog.AddFilter(filter1)

	filter2, _ := gtk.FileFilterNew()
	filter2.SetName("All files")
	filter2.AddPattern("*")
	dialog.AddFilter(filter2)

	wd, err := os.Getwd()
	if err != nil {
		fmt.Println("couldnt get wd (how???)")
		return
	}
	dialog.FileChooser.SetCurrentFolder(wd)

	defer dialog.Destroy()

	if dialog.Run() == gtk.RESPONSE_OK {
		filename := dialog.GetFilename()

		file, err := os.Open(filename)
		if err != nil {
			return // TODO error handling
		}
		defer file.Close()

		writing, _ := io.ReadAll(file)

		text.PageLoaded(string(writing), page.GetCurrentPage().View.Scale)
	}
}

func (app *App) AboutDialog() {
	obj, _ := app.builder.GetObject("aboutDialog")
	dialog := obj.(*gtk.AboutDialog)

	obj, _ = app.builder.GetObject("aboutDialogButtonBox")
	bb := obj.(*gtk.ButtonBox)

	if bb.GetChildren().Length() > 2 {
		data := bb.GetChildren().NthData(2) // Close button
		closeBut := data.(*gtk.Widget)
		closeBut.Connect("clicked", func() { dialog.Hide() })
	}

	dialog.Run()
}

func (app *App) MarkdownHelpDialog() {
	obj, _ := app.builder.GetObject("markdownHelpDialog")
	dialog := obj.(*gtk.Dialog)

	obj, _ = app.builder.GetObject("markdownHelpDialogButtonClose")
	b := obj.(*gtk.Button)
	b.Connect("clicked", func() { dialog.Hide() })

	dialog.Run()
}

func (app *App) ThicknessDialog() {
	obj, _ := app.builder.GetObject("thicknessDialog")
	dialog := obj.(*gtk.Dialog)

	obj, _ = app.builder.GetObject("thicknessScaleAdjustment")
	adj := obj.(*gtk.Adjustment)
	adj.SetValue(dAreaThicknessThickness)

	obj, _ = app.builder.GetObject("thicknessDialogButtonClose")
	bc := obj.(*gtk.Button)
	bc.Connect("clicked", func() {
		dAreaThicknessThickness = adj.GetValue()
		dialog.Hide()
	})

	dialog.Run()
}

func (app *App) DAreaEnter(widget *gtk.DrawingArea, event *gdk.Event) {
	app.DAreaUpdateCursor(widget)
}

func (app *App) DAreaUpdateCursor(widget *gtk.DrawingArea) {
	display, _ := gdk.DisplayGetDefault()

	var name string
	switch page.GetCurrentTool() {
	case types.ToolText:
		name = "text"
	default:
		name = "crosshair"
	}

	if drawing.IsSecondaryButtonHeld {
		name = "move"
	}

	cursor, _ := gdk.CursorNewFromName(display, name)

	parent, _ := widget.GetParentWindow()
	parent.SetCursor(cursor)
}

func (app *App) DAreaLeave(widget *gtk.DrawingArea, event *gdk.Event) {
	display, _ := gdk.DisplayGetDefault()
	cursor, _ := gdk.CursorNewFromName(display, "default")

	parent, _ := widget.GetParentWindow()
	parent.SetCursor(cursor)
}

func (app *App) ExportPNG(window *gtk.ApplicationWindow) {
	const margin float64 = 20

	file := filehandling.GetCurrentFile() + ".png"

	page.GetCurrentPage().FindSize()

	view := types.View{
		Position: types.Coords2D{
			X: page.GetCurrentPage().Smallest.X - margin,
			Y: page.GetCurrentPage().Smallest.Y - margin,
		},
		Scale: 1.0,
	}

	width := page.GetCurrentPage().Size.X + 2*margin
	height := page.GetCurrentPage().Size.Y + 2*margin

	settings := *page.GetCurrentSettings()
	page := *page.GetCurrentPage() // make a copy so it doesn't break

	obj, _ := app.builder.GetObject("exportPNGWindow")
	win := obj.(*gtk.Window)
	obj, _ = app.builder.GetObject("exportLabel")
	lbl := obj.(*gtk.Label)
	obj, _ = app.builder.GetObject("exportSpinner")
	spinner := obj.(*gtk.Spinner)

	lbl.SetLabel("Exporting...")
	win.ShowAll()

	done := make(chan bool)

	// render async
	go func() {
		surf := cairo.CreateImageSurface(cairo.FORMAT_ARGB32, int(width), int(height))
		cr := cairo.Create(surf)

		drawing.Render(cr, &page, &settings, false, &view, width, height)

		surf.WriteToPNG(file)

		done <- true
	}()

	glib.TimeoutAdd(200, func() { app.exportPNGWinTimeout(win, lbl, spinner, file, done) })
}

func (app *App) exportPNGWinTimeout(win *gtk.Window, lbl *gtk.Label, spinner *gtk.Spinner, file string, done chan bool) {
	select {
	case <-done:
		s := strings.Split(file, "/")
		lbl.SetLabel("Exported as " + s[len(s)-1])
		spinner.Hide()
		close(done)
		glib.TimeoutAdd(1500, func() { win.Hide() })
	default:
		glib.TimeoutAdd(200, func() { app.exportPNGWinTimeout(win, lbl, spinner, file, done) })
	}
}

func (app *App) ExportMD() {
	fn := filehandling.GetCurrentFile() + ".md"
	tvBuf, _ := app.textView.GetBuffer()
	text, _ := tvBuf.GetText(tvBuf.GetStartIter(), tvBuf.GetEndIter(), true)

	file, err := os.Create(fn)
	if err != nil {
		return // TODO
	}

	file.Write([]byte(text))
	file.Close()

	obj, _ := app.builder.GetObject("bottomLabel")
	obj.(*gtk.Label).SetLabel("Exported as Markdown.")
	glib.TimeoutAdd(1000, func() { app.UpdateViewLabel(&page.GetCurrentPage().View) })
}

func (app *App) GridDialog() {
	settings := page.GetCurrentSettings()

	obj, _ := app.builder.GetObject("gridDialog")
	dialog := obj.(*gtk.Dialog)

	obj, _ = app.builder.GetObject("gridDialogIsActiveCheckButton")
	activeCB := obj.(*gtk.CheckButton)
	obj, _ = app.builder.GetObject("gridDialogSnapCheckButton")
	snapCB := obj.(*gtk.CheckButton)
	obj, _ = app.builder.GetObject("gridSizeScaleAdjustment")
	sizeA := obj.(*gtk.Adjustment)
	obj, _ = app.builder.GetObject("gridDialogSizeEntry")
	sizeE := obj.(*gtk.Entry)

	activeCB.SetActive(settings.GridActive)
	snapCB.SetActive(settings.GridSnap)
	sizeA.SetValue(settings.GridSize)

	obj, _ = app.builder.GetObject("gridDialogCloseButton")
	obj.(*gtk.Button).Connect("clicked", func() { dialog.Hide() })

	dialog.Run()

	settings.GridActive = activeCB.GetActive()
	settings.GridSnap = snapCB.GetActive()
	settings.GridSize = sizeA.GetValue()

	entryText, _ := sizeE.GetText()
	sz, err := strconv.ParseFloat(entryText, 64)
	if err == nil {
		settings.GridSize = sz
	}
}
