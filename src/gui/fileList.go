package gui

import (
	"fmt"
	"strings"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/filehandling"
	"gitlab.com/Jerrynicki/notes/src/page"
	"gitlab.com/Jerrynicki/notes/src/text"
)

func (app *App) UpdateFileList() {
	obj, _ := app.builder.GetObject("listBoxFiles")
	listBox := obj.(*gtk.ListBox)

	files, err := filehandling.GetFilesInCurDir()
	if err != nil {
		// TODO
		return
	}

	app.ClearListBox()

	buttonBack, _ := gtk.ButtonNewWithLabel("←")
	buttonBack.SetTooltipText("Navigate to above directory")
	buttonBack.Connect("clicked", func() { app.FileButtonClicked("..", true) })
	listBox.Add(buttonBack)

	for _, f := range files {
		var nameDisplay string
		var tooltip string
		var name string
		var isDir bool

		name = f.Name()
		isDir = f.IsDir()

		if isDir {
			nameDisplay = "[" + name + "]"
			tooltip = "Navigate into directory <tt>" + name + "</tt>"
		} else {
			if !strings.HasSuffix(name, ".note") {
				continue
			}

			nameDisplay = name
			tooltip = "Open file <tt>" + name + "</tt>"
		}

		button, _ := gtk.ButtonNewWithLabel(nameDisplay)

		button.SetTooltipMarkup(tooltip)

		button.Connect("clicked", func() { app.FileButtonClicked(name, isDir) })
		listBox.Add(button)
	}

	buttonNew, _ := gtk.ButtonNewWithLabel("+")
	buttonNew.SetTooltipText("Create a new file")
	buttonNew.Connect("clicked", func() { app.FileButtonClicked("+", false) })
	listBox.Add(buttonNew)

	listBox.ShowAll()
}

func (app *App) FileButtonClicked(name string, isDir bool) {
	if isDir {
		if name == ".." { // go up one level
			dirSplit := strings.Split(filehandling.GetCurrentDir(), "/")
			filehandling.SetCurrentDir(strings.Join(dirSplit[:len(dirSplit)-1], "/"))
		} else {
			filehandling.SetCurrentDir(filehandling.GetCurrentDir() + "/" + name)
		}

		fmt.Println(filehandling.GetCurrentDir())
	} else {
		if name == "+" { // new file
			page.MakeEmptyPage()
			filehandling.SetCurrentFile("")
			text.PageLoaded("", page.GetCurrentPage().View.Scale)
		} else {
			filename := filehandling.GetCurrentDir() + "/" + name

			newPage, err := filehandling.LoadPageFromFilename(filename)
			if err != nil {
				return
			}

			page.SetCurrentPage(newPage)
			text.PageLoaded(newPage.Writing, newPage.View.Scale)
			filehandling.SetCurrentFile(filename)
			app.QueueDrawAreaDraw()
		}
	}

	app.UpdateFileList()
	app.QueueDrawAreaDraw()
}

func (app *App) ClearListBox() {
	obj, _ := app.builder.GetObject("listBoxFiles")
	listBox := obj.(*gtk.ListBox)

	for {
		row := listBox.GetRowAtIndex(0)
		if row == nil {
			break
		}
		row.Destroy()
	}

}
