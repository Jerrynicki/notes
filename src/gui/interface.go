package gui

import (
	"fmt"
	"strings"
	"time"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/config"
	"gitlab.com/Jerrynicki/notes/src/filehandling"
	"gitlab.com/Jerrynicki/notes/src/types"
)

var (
	lastDraw time.Time
)

func (app *App) UpdateViewLabel(view *types.View) {
	f := filehandling.GetCurrentFile()
	if f == "" {
		f = "(no file)"
	}

	s := strings.Split(f, "/")

	obj, _ := app.builder.GetObject("bottomLabel")
	obj.(*gtk.Label).SetLabel(
		fmt.Sprintf("Position: %.0f %.0f | Zoom: %.0f%% | %s", view.Position.X, view.Position.Y, view.Scale*100, s[len(s)-1]),
	)
}

func (app *App) UpdateBottomLabel2(s string) {
	obj, _ := app.builder.GetObject("bottomLabel2")
	obj.(*gtk.Label).SetLabel(s)
}

func (app *App) GetSelectedColor() types.Color {
	var c types.Color
	obj, _ := app.builder.GetObject("colorButtonFg")
	gdkColor := obj.(*gtk.ColorButton).GetRGBA()
	c.FromFloat64(gdkColor.GetRed(), gdkColor.GetGreen(), gdkColor.GetBlue(), gdkColor.GetAlpha())

	return c
}

func (app *App) GetSelectedBgColor() types.Color {
	var c types.Color

	obj, _ := app.builder.GetObject("colorButtonBg")
	gdkColor := obj.(*gtk.ColorButton).GetRGBA()
	c.FromFloat64(gdkColor.GetRed(), gdkColor.GetGreen(), gdkColor.GetBlue(), gdkColor.GetAlpha())

	return c
}

func (app *App) GetSelectedFont() string {
	return "Helvetica 14"
}

func (app *App) GetSelectedThickness() float64 {
	return float64(dAreaThicknessThickness)
}

func (app *App) QueueDrawAreaDraw() {
	app.drawingArea.QueueDraw()
}

func (app *App) QueueDrawAreaDrawUnimportant() {
	tm := time.Now()
	d := time.Duration(config.GetConfig().MinDurationBetweenUnimportantDraws) * time.Millisecond
	if tm.Sub(lastDraw) >= d {
		app.drawingArea.QueueDraw()
		lastDraw = tm
	}
}

func (app *App) GetTextView() *gtk.TextView {
	return app.textView
}
