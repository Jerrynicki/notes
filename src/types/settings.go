package types

const (
	ToolPen int = iota
	ToolLine
	ToolCircleCenter
	ToolCircleEnds
	ToolRectangle
	ToolText
	ToolArrow
	ToolEraser
)

type Settings struct {
	Thickness            float64
	MinimumPointDistance float64
	Tool                 int
	AutoMove             bool
	GridActive           bool
	GridSnap             bool
	GridSize             float64
	GridColor            Color
}
