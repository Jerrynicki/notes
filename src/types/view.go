package types

type View struct {
	// The Page view, holds current "camera" position on the page and scale (0-1)
	Position Coords2D
	Scale    float64
}

func (view *View) TransformCoords(c Coords2D) Coords2D {
	// Transforms coordinates to screenspace coordinates, applying the current view
	result := new(Coords2D)
	result.X = (c.X - view.Position.X) * view.Scale
	result.Y = (c.Y - view.Position.Y) * view.Scale

	return *result
}

func (view *View) TransformCoordsBack(c Coords2D) Coords2D {
	// Transforms screenspace coordinates back to canvas coordinates, applying the current view
	result := new(Coords2D)

	result.X = c.X/view.Scale + view.Position.X
	result.Y = c.Y/view.Scale + view.Position.Y

	return *result
}
