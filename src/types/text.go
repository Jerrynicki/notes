package types

type Text struct {
	// A text
	// The position is relative to the Page
	Label    string
	Color    Color
	Font     Font
	Size     Coords2D
	Position Coords2D
}
