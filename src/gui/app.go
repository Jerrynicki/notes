package gui

import (
	"os"
	"path/filepath"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/config"
	"gitlab.com/Jerrynicki/notes/src/drawing"
	"gitlab.com/Jerrynicki/notes/src/filehandling"
	"gitlab.com/Jerrynicki/notes/src/page"
	"gitlab.com/Jerrynicki/notes/src/text"
	"gitlab.com/Jerrynicki/notes/src/types"
)

var (
	standardColor           = types.Color{R: 0xff, G: 0xff, B: 0xff, A: 0xff}
	standardBgColor         = types.Color{R: 0x2d, G: 0x2d, B: 0x2d, A: 0xaa}
	dAreaThicknessThickness = 2.0
)

type App struct {
	application          *gtk.Application
	window               *gtk.ApplicationWindow
	drawingArea          *gtk.DrawingArea
	drawingAreaThickness *gtk.DrawingArea
	textView             *gtk.TextView
	builder              *gtk.Builder
	scrsize              types.Coords2D
}

func (app *App) InitInterfaces() {
	drawing.LinkApp(drawing.AppInterface(app))
	text.LinkApp(text.AppInterface(app))
	filehandling.LinkApp(filehandling.AppInterface(app))

	// FIXME
	d, _ := os.Getwd()
	filehandling.SetCurrentDir(d)
}

func (app *App) OnActivate(application *gtk.Application) {
	var obj glib.IObject
	var err error

	app.application = application

	ex, _ := os.Executable()
	builder, err := gtk.BuilderNewFromFile(filepath.Dir(ex) + "/" + config.GetConfig().GladeDir + "/notes.glade")
	if err != nil {
		panic(err)
	}

	// Get app-wide objects

	app.builder = builder

	obj, _ = builder.GetObject("window")
	app.window = obj.(*gtk.ApplicationWindow)

	obj, _ = builder.GetObject("drawingArea")
	app.drawingArea = obj.(*gtk.DrawingArea)

	application.AddWindow(app.window)

	// Find size of default monitor, set size to screensize/1.5
	displ, _ := app.window.GetDisplay()
	defaultscr, _ := displ.GetDefaultScreen()
	monitor, _ := displ.GetMonitor(defaultscr.GetScreenNumber())
	geo := monitor.GetGeometry()
	app.scrsize.X = float64(geo.GetWidth())
	app.scrsize.Y = float64(geo.GetHeight())

	app.window.SetDefaultSize(int(app.scrsize.X/1.5), int(app.scrsize.Y/1.5))

	// Connections and events

	// DrawingArea
	app.drawingArea.Connect("draw", drawing.OnDraw)

	obj, _ = builder.GetObject("menuFileOpen")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.LoadPage(app.window) })
	obj, _ = builder.GetObject("menuFileSaveAs")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.SavePageAs(app.window) })
	obj, _ = builder.GetObject("menuFileSave")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.SavePage(app.window) })

	obj, _ = builder.GetObject("menuFileImportMD")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.ImportMD() })

	obj, _ = builder.GetObject("menuFileExportPNG")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.ExportPNG(app.window) })
	obj, _ = builder.GetObject("menuFileExportMD")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.ExportMD() })

	obj, _ = builder.GetObject("menuViewReset")
	obj.(*gtk.MenuItem).Connect("activate", func() {
		page.GetCurrentPage().View.Position.X = 0
		page.GetCurrentPage().View.Position.Y = 0
		page.GetCurrentPage().View.Scale = 1
		app.QueueDrawAreaDraw()
	})

	obj, _ = builder.GetObject("menuViewGrid")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.GridDialog() })

	obj, _ = builder.GetObject("buttonToolPen")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolPen) })
	obj, _ = builder.GetObject("buttonToolLine")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolLine) })
	obj, _ = builder.GetObject("buttonToolArrow")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolArrow) })
	obj, _ = builder.GetObject("buttonToolRectangle")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolRectangle) })
	obj, _ = builder.GetObject("buttonToolCircleCenter")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolCircleCenter) })
	obj, _ = builder.GetObject("buttonToolCircleEnds")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolCircleEnds) })
	obj, _ = builder.GetObject("buttonToolEraser")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolEraser) })
	obj, _ = builder.GetObject("buttonToolText")
	obj.(*gtk.Button).Connect("clicked", func() { drawing.ToolButtonEvent(types.ToolText) })

	obj, _ = builder.GetObject("drawingAreaThickness")
	app.drawingAreaThickness = obj.(*gtk.DrawingArea)
	app.drawingAreaThickness.AddEvents(int(gdk.SCROLL_MASK))
	app.drawingAreaThickness.AddEvents(int(gdk.BUTTON_PRESS_MASK))
	app.drawingAreaThickness.AddEvents(int(gdk.BUTTON_RELEASE_MASK))
	app.drawingAreaThickness.Connect("scroll-event", app.DAreaThicknessScroll)
	app.drawingAreaThickness.Connect("button-release-event", func() { app.ThicknessDialog() })
	app.drawingAreaThickness.Connect("draw", app.DAreaThicknessDraw)

	app.drawingArea.AddEvents(int(gdk.BUTTON_PRESS_MASK))
	app.drawingArea.AddEvents(int(gdk.BUTTON_RELEASE_MASK))
	app.drawingArea.AddEvents(int(gdk.POINTER_MOTION_MASK))
	app.drawingArea.AddEvents(int(gdk.SCROLL_MASK))
	app.drawingArea.AddEvents(int(gdk.ENTER_NOTIFY_MASK))
	app.drawingArea.AddEvents(int(gdk.LEAVE_NOTIFY_MASK))
	app.drawingArea.Connect("button-press-event", drawing.ButtonPressEvent)
	app.drawingArea.Connect("button-release-event", drawing.ButtonReleaseEvent)
	app.drawingArea.Connect("motion-notify-event", drawing.MotionNotifyEvent)
	app.drawingArea.Connect("scroll-event", drawing.ScrollEvent)
	app.drawingArea.Connect("enter-notify-event", app.DAreaEnter)
	app.drawingArea.Connect("leave-notify-event", app.DAreaLeave)

	app.window.AddEvents(int(gdk.KEY_PRESS_MASK))
	app.window.Connect("key-press-event", func(w *gtk.ApplicationWindow, event *gdk.Event) { drawing.KeyEvent(app.drawingArea, event) })

	// Text
	obj, _ = builder.GetObject("textView")
	app.textView = obj.(*gtk.TextView)

	app.window.AddEvents(int(gdk.KEY_PRESS_MASK))
	app.window.AddEvents(int(gdk.KEY_RELEASE_MASK))
	app.textView.Connect("key-press-event", text.KeyPressEvent)
	app.textView.Connect("key-release-event", text.KeyReleaseEvent)
	app.textView.Connect("move-cursor", text.TextViewMoveCursor)

	buf, _ := app.textView.GetBuffer()
	buf.Connect("changed", func() { text.TextBufferChanged(app.textView, buf) })

	text.InitTags(buf)

	//Menu
	obj, _ = builder.GetObject("menuHelpAbout")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.AboutDialog() })
	obj, _ = builder.GetObject("menuHelpMarkdown")
	obj.(*gtk.MenuItem).Connect("activate", func() { app.MarkdownHelpDialog() })

	// FIXME
	app.UpdateFileList()

	app.window.ShowAll()
}
