package types

type Color struct {
	R uint8
	G uint8
	B uint8
	A uint8
}

const maxUInt8 = float64(^uint8(0))

func (color *Color) ToFloat64() (float64, float64, float64, float64) {
	// Converts the color from uint8 to float64
	// 255 255 255 255 -> 1 1 1 1
	// 0 0 0 0 -> 0 0 0 0
	// 128 128 128 128 -> 0.5 0.5 0.5 0.5

	return float64(color.R) / maxUInt8, float64(color.G) / maxUInt8, float64(color.B) / maxUInt8, float64(color.A) / maxUInt8
}

func (color *Color) FromFloat64(r float64, g float64, b float64, a float64) {
	// Overwrites the color, converting the given float64s to uint8s
	// 1 1 1 1 -> 255 255 255 255
	// 0 0 0 0 -> 0 0 0 0
	// 0.5 0.5 0.5 0.5 -> 127 127 127 127

	color.R = uint8(r * maxUInt8)
	color.G = uint8(g * maxUInt8)
	color.B = uint8(b * maxUInt8)
	color.A = uint8(a * maxUInt8)
}

func (color *Color) Inverted() Color {
	c := Color{}

	c.R = uint8(maxUInt8) - color.R
	c.G = uint8(maxUInt8) - color.G
	c.B = uint8(maxUInt8) - color.B

	return c
}
