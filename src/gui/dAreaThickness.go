package gui

import (
	"fmt"
	"math"

	"github.com/gotk3/gotk3/cairo"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/Jerrynicki/notes/src/types"
)

func (app *App) DAreaThicknessScroll(widget *gtk.DrawingArea, rawEvent *gdk.Event) {
	event := &gdk.EventScroll{Event: rawEvent} // workaround because gotk is weird

	if event.Direction() == gdk.SCROLL_UP {
		dAreaThicknessThickness = math.Min(float64(app.drawingAreaThickness.GetAllocatedHeight()/2), dAreaThicknessThickness+1)
	} else if event.Direction() == gdk.SCROLL_DOWN {
		dAreaThicknessThickness = math.Max(1, dAreaThicknessThickness-1)
	}

	app.drawingAreaThickness.QueueDraw()
}

func (app *App) DAreaThicknessDraw(widget *gtk.DrawingArea, cr *cairo.Context) {
	width := float64(widget.GetAllocatedWidth())
	height := float64(widget.GetAllocatedHeight())

	bg := app.GetSelectedBgColor()
	cr.SetSourceRGBA(bg.ToFloat64())
	cr.Rectangle(0, 0, width, height)
	cr.Fill()

	fg := app.GetSelectedColor()
	cr.SetSourceRGBA(fg.ToFloat64())
	cr.Arc(width/2, height/2, dAreaThicknessThickness, 0, 2*math.Pi)
	cr.Fill()

	obg := types.Color{R: 255 - bg.R, G: 255 - bg.G, B: 255 - bg.B, A: 255} // opposite of bg color
	cr.SetSourceRGBA(obg.ToFloat64())
	cr.MoveTo(5, 5+height/6)
	cr.SelectFontFace("monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
	cr.SetFontSize(height / 6)
	cr.ShowText(fmt.Sprint(dAreaThicknessThickness))
}
