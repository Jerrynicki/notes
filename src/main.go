package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"

	"gitlab.com/Jerrynicki/notes/src/config"
	"gitlab.com/Jerrynicki/notes/src/gui"
	"gitlab.com/Jerrynicki/notes/src/page"
)

const (
	configLocation = "config.json"
)

var (
	app         *gui.App
	application *gtk.Application
)

func main() {
	const appID = "org.jrery.notes"

	ex, _ := os.Executable() // check for config at the path of the executable
	fmt.Println(filepath.Dir(ex))
	err := config.ReadConfig(filepath.Dir(ex) + "/" + configLocation)

	if err != nil {
		fmt.Print("Config could not be read, would you like to create a default config file at config.json? (y/N) ")
		var ans string
		fmt.Scanf("%s", &ans)

		if strings.ToLower(ans) == "y" {
			config.CreateDefaultConfig(filepath.Dir(ex) + "/" + configLocation)
		} else {
			fmt.Println("why not :(")
		}

		config.ApplyDefaultConfig()
	}

	page.MakeEmptyPage()

	application, err = gtk.ApplicationNew(appID, glib.APPLICATION_FLAGS_NONE)
	if err != nil {
		panic(err)
	}

	app = new(gui.App)
	app.InitInterfaces()
	application.Connect("activate", func() { app.OnActivate(application) })

	os.Exit(application.Run(os.Args))
}
