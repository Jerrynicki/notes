package config

import (
	"encoding/json"
	"os"
)

var (
	defaultConf = Config{
		MinDurationBetweenUnimportantDraws: 33,
		GladeDir:                           "glade",
	}

	cfg *Config
)

type Config struct {
	MinDurationBetweenUnimportantDraws int
	GladeDir                           string
}

func ReadConfig(fp string) error {
	c := new(Config)

	f, err := os.Open(fp)
	if err != nil {
		return err
	}
	defer f.Close()

	decoder := json.NewDecoder(f)
	decoder.Decode(c)

	cfg = c

	return nil
}

func ApplyDefaultConfig() {
	cfg = &defaultConf
}

func CreateDefaultConfig(fp string) error {
	f, err := os.Create(fp)
	if err != nil {
		return err
	}
	defer f.Close()

	encoder := json.NewEncoder(f)
	encoder.Encode(defaultConf)

	return nil
}

func GetConfig() *Config {
	return cfg
}
