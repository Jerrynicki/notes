package page

import (
	"gitlab.com/Jerrynicki/notes/src/types"
)

var (
	currentPage     *types.Page
	currentSettings *types.Settings
)

func MakeEmptyPage() {
	currentPage = new(types.Page)
	currentPage.BackgroundColor = types.Color{R: 255, G: 255, B: 255, A: 255}
	currentPage.View.Scale = 1

	currentSettings = new(types.Settings)
	currentSettings.Thickness = 2
	currentSettings.MinimumPointDistance = 3
	currentSettings.AutoMove = true
}

func GetCurrentPage() *types.Page {
	return currentPage
}

func SetCurrentPage(page *types.Page) {
	currentPage = page
	// TODO: drawareadraw in loading code
}

func SetPageWriting(writing string) {
	currentPage.Writing = writing
}

func GetCurrentTool() int {
	return currentSettings.Tool
}

func GetCurrentSettings() *types.Settings {
	return currentSettings
}
