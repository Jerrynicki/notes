package text

import (
    "fmt"

    "github.com/gotk3/gotk3/gdk"
    "github.com/gotk3/gotk3/gtk"
    "gitlab.com/Jerrynicki/notes/src/page"
)

const (
    ctrlKeycode uint = 65507
)

var (
    ctrlHeld bool
)

func TextBufferChanged(widget *gtk.TextView, buf *gtk.TextBuffer) {
    text, err := buf.GetText(buf.GetStartIter(), buf.GetEndIter(), true)
    if err == nil {
        page.SetPageWriting(text)
    }

    SetGlobalScale(page.GetCurrentPage().View.Scale, buf)
    RenderMarkdown(widget, buf)
}

func TextViewMoveCursor(widget *gtk.TextView, step int, count int, extendSelection bool) {
    fmt.Println("yo", step, count, extendSelection)
}

func KeyPressEvent(widget *gtk.TextView, rawEvent *gdk.Event) {
    event := gdk.EventKey{Event: rawEvent}

    if ctrlHeld {
        unicode := gdk.KeyvalToUnicode(event.KeyVal())
        buf, _ := widget.GetBuffer()

        // FIXME
        p := page.GetCurrentPage()
        if unicode == '+' {
            p.View.Scale += 0.1
        } else if unicode == '-' {
            p.View.Scale -= 0.1
        }
        app.UpdateViewLabel(&p.View)
        SetGlobalScale(p.View.Scale, buf)
        RenderMarkdown(widget, buf)
    }

    if event.KeyVal() == ctrlKeycode {
        ctrlHeld = true
    }
}

func KeyReleaseEvent(widget *gtk.TextView, rawEvent *gdk.Event) {
    event := gdk.EventKey{Event: rawEvent}

    if event.KeyVal() == ctrlKeycode {
        ctrlHeld = false
    }
}

func PageLoaded(text string, scale float64) {
    t := app.GetTextView()
    buf, _ := t.GetBuffer()
    buf.SetText(text)

    SetGlobalScale(scale, buf)
}
