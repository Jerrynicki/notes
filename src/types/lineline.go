package types

type LineLine struct {
	// A drawn line, made up from smaller Lines, which has a color and thickness
	// The position is relative to the Drawing
	Points    []LinePoint
	Size      Coords2D
	Position  Coords2D
	Color     Color
	Thickness float64
}

func (lineLine *LineLine) FindPosition() {
	// Finds the upper-left-most point in the line and sets the lineLines position to it
	var smallest Coords2D
	if len(lineLine.Points) > 0 {
		smallest = lineLine.Points[0].Position
	} else {
		smallest = Coords2D{}
	}

	for _, x := range lineLine.Points {
		if x.Position.X < smallest.X {
			smallest.X = x.Position.X
		}

		if x.Position.Y < smallest.Y {
			smallest.Y = x.Position.Y
		}
	}

	lineLine.Position = smallest
}

func (lineLine *LineLine) FindSize() {
	// Finds the lower-right-most point in the line and combines it with the upper-left-most point (position) to
	// set the size
	// If no position is defined yet, FindPosition() will be called

	largest := Coords2D{}
	for _, x := range lineLine.Points {
		if x.Position.X > largest.X {
			largest.X = x.Position.X
		}

		if x.Position.Y > largest.Y {
			largest.Y = x.Position.Y
		}
	}

	if lineLine.Position.X == 0 && lineLine.Position.Y == 0 { // Position not found yet
		lineLine.FindPosition()
	}

	lineLine.Size = Coords2D{largest.X - lineLine.Position.X, largest.Y - lineLine.Position.Y}
}
