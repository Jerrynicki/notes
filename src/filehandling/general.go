package filehandling

import (
	"io/fs"
	"io/ioutil"
)

var (
	currentDir  string
	currentFile string
)

func SetCurrentDir(s string) {
	currentDir = s
}

func SetCurrentFile(s string) {
	currentFile = s
}

func GetCurrentDir() string {
	return currentDir
}

func GetCurrentFile() string {
	return currentFile
}

func GetFilesInCurDir() ([]fs.FileInfo, error) {
	return ioutil.ReadDir(currentDir)
}
