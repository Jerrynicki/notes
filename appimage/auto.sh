rm -r AppDir
rm -r appimage-builder-cache
rm AppImageBuilder.yml

mkdir -p AppDir/usr/share/notes
mkdir -p AppDir/usr/bin

cd ../src
go build -o notes main.go
mv notes ../appimage/AppDir/usr/bin/app
cp -r glade/ ../appimage/AppDir/usr/share/notes/glade/

cd ../appimage/AppDir/usr/bin
echo '{"MinDurationBetweenUnimportantDraws": 33, "GladeDir": "../share/notes/glade"}' > config.json

cd ../../../

appimage-builder --generate
appimage-builder --recipe AppImageBuilder.yml --skip-test
