package types

type LinePoint struct {
	// The smallest "line" unit, has coordinates, used to make up bigger LineLines
	Position Coords2D
}
