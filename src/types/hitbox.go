package types

type Hitbox struct {
	TL Coords2D
	BR Coords2D
}

func (hitbox *Hitbox) FromCoordsAndSize(c Coords2D, s Coords2D) {
	TL := c
	BR := c.Add(&s)

	hitbox.From2Coords(TL, BR)
}

func (hitbox *Hitbox) From2Coords(c1 Coords2D, c2 Coords2D) {
	var smallest Coords2D
	var largest Coords2D

	smallest = c1
	largest = c1

	// make sure ordering is correct
	if c2.X < smallest.X {
		smallest.X = c2.X
	}
	if c2.Y < smallest.Y {
		smallest.Y = c2.Y
	}

	if c2.X > largest.X {
		largest.X = c2.X
	}
	if c2.Y > largest.Y {
		largest.Y = c2.Y
	}

	hitbox.TL = smallest
	hitbox.BR = largest
}

func (h1 *Hitbox) Intersects(h2 *Hitbox) bool {
	h1Left := h1.TL.X
	h1Right := h1.BR.X
	h1Top := h1.TL.Y
	h1Bottom := h1.BR.Y

	h2Left := h2.TL.X
	h2Right := h2.BR.X
	h2Top := h2.TL.Y
	h2Bottom := h2.BR.Y

	// https://gamedev.stackexchange.com/questions/586/what-is-the-fastest-way-to-work-out-2d-bounding-box-intersection
	return !(h2Left > h1Right || h2Right < h1Left || h2Top > h1Bottom || h2Bottom < h1Top)
}

func (hitbox *Hitbox) Contains(coords *Coords2D) bool {
	return (coords.X > hitbox.TL.X && coords.X < hitbox.BR.X) && (coords.Y > hitbox.TL.Y && coords.Y < hitbox.BR.Y)
}
