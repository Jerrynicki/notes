package types

import (
	"math"
)

type Coords2D struct {
	// A simple 2D coordinates type
	X float64
	Y float64
}

func (p1 *Coords2D) Add(p2 *Coords2D) Coords2D {
	// Adds p1 and p2 and returns the result
	result := new(Coords2D)
	result.X = p1.X + p2.X
	result.Y = p1.Y + p2.Y

	return *result
}

func (p1 *Coords2D) Subtract(p2 *Coords2D) Coords2D {
	// Subtracts p2 from p1 and returns the result
	result := new(Coords2D)
	result.X = p1.X - p2.X
	result.Y = p1.Y - p2.Y

	return *result
}

func (p1 *Coords2D) Product(p2 *Coords2D) Coords2D {
	// Multiplies both x and y values and returns the result
	result := new(Coords2D)
	result.X = p1.X * p2.X
	result.Y = p1.Y * p2.Y

	return *result
}

func (p1 *Coords2D) DistanceFrom(p2 *Coords2D) float64 {
	// Returns the distance between point a and point b
	return math.Abs(math.Sqrt(math.Pow(p2.X-p1.X, 2) + math.Pow(p2.Y-p1.Y, 2)))
}

func (c *Coords2D) IsInbounds(x1 float64, y1 float64, x2 float64, y2 float64) bool {
	// Checks if coords are in range (x1, y1, x2, y2)
	return !(c.X < x1 || c.X > x2 || c.Y < y1 || c.Y > y2)
}
